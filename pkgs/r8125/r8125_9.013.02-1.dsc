-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: r8125
Binary: r8125-dkms
Architecture: all
Version: 9.013.02-1
Maintainer: Hideki Yamane <henrich@debian.org>
Uploaders:  Andreas Beckmann <anbe@debian.org>,
Homepage: https://www.realtek.com/Download/List?cate_id=584
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/r8125
Vcs-Git: https://salsa.debian.org/debian/r8125.git
Testsuite: autopkgtest-pkg-dkms
Build-Depends: debhelper-compat (= 13), dh-sequence-dkms
Package-List:
 r8125-dkms deb non-free/kernel optional arch=all
Checksums-Sha1:
 8d6dca52af34ab47d4641f1348e23a4579a72f2b 105923 r8125_9.013.02.orig.tar.bz2
 d012c2b06e6df588d37c78b9321f706319f60313 4260 r8125_9.013.02-1.debian.tar.xz
Checksums-Sha256:
 d36410ee99c956f250d9cd08340d8c36567d190f420a8ee128ff6e51225aac0c 105923 r8125_9.013.02.orig.tar.bz2
 eca26188c99d243d383abcd3701d2c833126258f68a475ade9afba2be9fe2c1b 4260 r8125_9.013.02-1.debian.tar.xz
Files:
 ef907e71a20fac98b0e82a578f07e38a 105923 r8125_9.013.02.orig.tar.bz2
 784519d16dd07c2377508226b4941f0d 4260 r8125_9.013.02-1.debian.tar.xz
Autobuild: yes

-----BEGIN PGP SIGNATURE-----

iQJEBAEBCAAuFiEE6/MKMKjZxjvaRMaUX7M/k1np7QgFAmaBLAkQHGFuYmVAZGVi
aWFuLm9yZwAKCRBfsz+TWentCB3sD/9Arx8tPesK02VZ9febNsxAZeAbaT4HXLEA
QAWsAerjtM3KtjOHFR+N/BthAA8gSYDVF7DFfz+8p1mWTnNpcU2FNwvdfjVHMUb6
k2B59q63QUG/VJS+5He5J3E3q36RXgiZ4Bzcv1PqEdXyFNB/nqAZgDbumiinE0h3
2bCaKbcAsRa4ONzoq05ba/TTR9YF7721+YtoaIvOtyV3k9sGFUommd2PfcJR/Ck4
UWd5jopPayn6kyAVwNQ5S9zPltbCi2sg8gq57vZpcUWVx/eIWBFU6N3cSPKk0kmu
wWIxVHsCshpYqdtdgC9OxU9aAqOmD5n7fBBsTHXDNE0pgsYr8mQvn5RIwlfVtNGZ
8PJNadt7Mks4gdRvu/SK11On8Zn6QQkV8iPc9RBdyU9Cqf+sNEFXmQNMujYd9ahC
aotmKXeFIC0+Qq/J40SqIPqXR/3mi63lQ/J5K9OKAlCLzocpWa1ZPVEtTgWvavjX
MdcHfKvZtVtYw0NwwcgBKuN7paCjOe2GN1qu5CzaK87RpF4H8sCZXM+xCl83Z9NW
232BsuSrJcKJqhFg27NB2hLZadQlfg3yiRg0/lfdAGpqEc9bZD3/CJhi5e4fV32F
65PP7sPGSKw4+MVaibw0pHvgQDBA7ub3wrRS+UsnH7I8DgvNPKIcnXhgp2eldtqr
3QyxwjxgFg==
=cZes
-----END PGP SIGNATURE-----
