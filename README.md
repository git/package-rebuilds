## Introduction

This repo is for rebuilds of (Debian) packages that normally are not maintained
by Proxmox directly.

Rebuilds are commonly used to fix some issue that users of Proxmox project
face.

## Repository Structure

There's a branch per Debian release that maps to the respective releases of
Proxmox projects.

The folder structure is:

```
. repository root
|-- README.md
`-- pkgs
    `-- ${source-package}
        `-- ${source-package}-${source-version}
```

This nested folder structure helps with containing the build artefacts to a
specific source and easier tracking of what version this is about.

## Workflow

### High-Level Overview

First get sources (with packaging) from Debian, e.g., with `dget` and implement
the required modifications to fix the issue you're rebuilding for. Then build +
test that and when done, push the git commits and upload the package(s) (or ask
the release team to do that).

### Step-by-step Guide

The following step-by-step guide is also considering the case when there is an
existing rebuild of an older upstream release or debian revision for a package.

While this consideration makes up a big part of the steps and is actually an
edge-case most of the time, it's still important to avoid re-introducing
regression.

Anyhow, here are the (somewhat) detailed steps required:

1. check https://tracker.debian.org/ for the package you want to re-build, it
   it isn't listed there then it's likely that this is the wrong repository to
   host it. After all, this repo is for rebuilds of existing packages with
   selected, specific fixes.
2. find the version you want to re-build, often that will be the one from the
   current 'stable' release, or 'oldstable', if the rebuild targets the release
   before that.
3. check if the source of that package is already checked in from a previous
   rebuild
   - If there is a source checked in for a **older** version you should check
     what it was rebuild for. If that is obsolete now (e.g., got upstreamed or
     work-arounded at some other level) you can delete the source directory and
     create a commit that documents why this is OK. E.g., with a reference to
     upstream or packaging changes fixing the original reason for the rebuild.
   - If there is a source checked in for the **current** upstream release
     matching the target distro (e.g., `stable`) then you should check if there
     were newer Debian revisions made, if not you can simply continue from the
     existing source. If there are newer Debian revisions you should download
     them first (see next steps) and merge in previous local changes from the
     previous rebuild, if they are still applicable.
   - If there is **no** source package yet, create it through something like
     `mkdir -p pkgs/${source-package}`
4. change into that directory and use `dget ${link}` to download the current
   and desired version of the source package.
   To get a clean state you should unapply local patches, e.g. through:
   ```
   cd ${source-package}-${source-version}
   quilt pop -a
   ```
5. If there was a previous existing re-build then check if you need to migrate
   the downstream changes of the last rebuild, and do so. Then add the current
    state, i.e. the extracted source plus the DSC file, both without any new
    debian patch changes or downstream changes, into git and commit that.
6. implement your (downstream) adaptions and commit them such that semantic
   changes are grouped together.
   - changes to packaging can be made directly on the sources
   - changes to upstream code might be better done through a (quilt) patch
     tracked in `debian/patch/series`, but this depends on the specific source
     package and the size of the changes, some small stuff might be also OK if
     done directly.
     If doing changes to upstream code yourself, i.e., not cherry-picked from
     some upstream development branch or proposed patch or pull-request, you
     should also consider if relaying that change to upstream makes sense
     (i.e., could this be helpful for others than Proxmox projects).
7. Add a new entry to the `debian/changelog` with a description of the changes
   (worded such that a user without programming experience can derive what's
   going on) and increase the revision part (after the `-`) of the version.
   The version should be increased such that:
   - It orders before the one in the repositories, as otherwise it won't be
     pulled in as update. For just packaging changes one could e.g. just add a
     `.1` or `+1` to the end of the existing version, for example: change
     `1.0-1` to `1.0-1+1`.
   - On major upgrades the one from the target is pulled in. This is important
     if you upgraded to a newer upstream version and for architecture specific
     packages . This can be achieved by using a `~bpoX` suffix. As `~` orders
     behind everything else, the version `1.0-1` will be favored over
     `1.0-1~bpo12+1`.
8. build it. This can be done directly through executing something like
   `dpkg-buildpackage -b -uc -us` or by creating a source-package first, e.g.,
   through `dpkg-buildpackage -S -uc -us -d` and then build that in a clean and
   contained way, like through `sbuild`.
9. If the package(s) got build, all changes got tested and reviewed, you're
   ready to push the changes to git and upload all binary package(s) that get
   build from this source package to the staging repository of the affected
   Proxmox projects. If unsure ask the Proxmox release team for help with
   that, as currently only they can modify the Debian repositories of Proxmox
   projects.
